\begin{UseCase}{CU6-1 Registrar Responsable}{}{
	Registrar los datos de un nuevo responsable dentro de la organización.
}
	\UCitem{Versi\'on}{2.0}
	\UCitem{Actor}{Director.}
	\UCitem{Prop\'osito}{Que el director sea capaz de registrar los atributos de un nuevo responsable}
	\UCitem{Resumen}{El sistema muestra los responsables dentro de la organización con las que interactuan las escuelas y la opción para que el Director sea capaz de registrar uno nuevo.}
	\UCitem{Entradas}{Nombre del nuevo responsable, puesto, extensión, celular, un correo institucional, un correo alterno y el tipo de responsable.}
	\UCitem{Salidas}{El sistema muestra mensaje de registro exitoso.}
	\UCitem{Precondiciones}{Tener los datos del \emph{Responsable} a ser registrado.}
	\UCitem{Postcondiciones}{Ninguna.}
	\UCitem{Autor}{Mejia Linares Oswaldo.}
\end{UseCase}


	\begin{UCtrayectoria}{Principal}
	\UCpaso[\UCactor] Selecciona el botón \IUbutton{Nuevo Registro} que aparece en el sistema via la \IUref{UI631}{Pantalla de Responsables}.
	\UCpaso Despliega la \IUref{UI124}{Pantalla de Registro}.
	\UCpaso[\UCactor] Anota los valores del nuevo usuario. \Trayref{A}
	\UCpaso[\UCactor] Selecciona el botón \IUbutton{Registrar} cuando haya finalizado. \Trayref{B}
	\UCpaso Despliega el mensaje {\bf MSG3-}``¿Desea guardar el nuevo registro?''.
	\UCpaso[\UCactor] Selecciona la opción de \IUbutton {Aceptar}. \Trayref{C}
	\UCpaso Verifica la validez de los datos proporcionados con base en el modelo de datos.
	\UCpaso Almacena la información en la BD. \Trayref{D}.
	\UCpaso Despliega el mensaje {\bf MSG2-}``Responsable creado correctamente''.
	\UCpaso Despliega la \IUref{UI123}{Pantalla de Información}.
\end{UCtrayectoria}
	
\begin{UCtrayectoriaA}{A}{Cancelar registro}
		\UCpaso Muestra el Mensaje {\bf MSG1-}``¿Estás seguro que desea cancelar el registro? Los datos no se guardarán''. 
		\UCpaso [\UCactor] Da click en el botón \IUbutton{Aceptar}.
		\UCpaso Envía al actor a la \IUref{UI123}{Pantalla de Responsables}, en caso de otra selección, permanece en la misma pantalla.
	\end{UCtrayectoriaA}
	
	\begin{UCtrayectoriaA}{B}{Datos erroneos o incompletos}
	    \UCpaso Muestra el Mensaje {\bf MSG4-}``Datos erroneos o incompletos, anote de forma correcta todos los campos''. 
		\UCpaso [\UCactor] Da click en el botón \IUbutton{Aceptar}.
		\UCpaso Permanece en la misma pantalla.
	\end{UCtrayectoriaA}
	
	\begin{UCtrayectoriaA}{C}{El usuario da clic en el botón \IUbutton{Cancelar}}
		\UCpaso Permanece en la misma pantalla.
	\end{UCtrayectoriaA}
	
	\begin{UCtrayectoriaA}{D}{Error en almacenar los datos}
		\UCpaso Muestra el Mensaje {\bf MSG4-}``Error en la conexión, vuelva a intentarlo más tarde''.
	\end{UCtrayectoriaA}